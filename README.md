# TODO minimalist REST API

In memory minimalist REST API

## Run

```bash
go run .
```

## Routes

Take care, trailing slash are mandatory

```bash
curl --request GET \
  http://localhost:8080/todo/
```

```bash
curl --request GET \
  http://localhost:8080/todo/{id}/
```

```bash
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"text": "new text"}' \
  http://localhost:8080/todo/
```

```bash
curl --header "Content-Type: application/json" \
  --request PUT \
  --data '{"text": "updated text"}' \
  http://localhost:8080/todo/{id}/
```

```bash
curl --request DELETE \
  http://localhost:8080/todo/{id}/
```
