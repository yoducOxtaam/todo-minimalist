package main

import (
	"errors"
	"net/http"
	"slices"
	"strconv"

	jsoniter "github.com/json-iterator/go"
)

type todo struct {
	ID   int    `json:"id"`
	Text string `json:"text"`
	Done bool   `json:"done"`
}

type TodoManager interface {
	Set(in todo) int
	Update(id int, in todo)
	Delete(id int)
	GetById(id int) (todo, error)
	GetAll() []todo
}

type todoManager struct {
	todos map[int]todo
	id    int
}

func NewTodoManager() TodoManager {
	return &todoManager{
		id:    1,
		todos: map[int]todo{},
	}
}

func (manager *todoManager) Set(in todo) int {
	id := manager.id
	in.ID = id
	manager.todos[id] = in
	manager.id++
	return id
}

func (manager *todoManager) Update(id int, in todo) {
	in.ID = id
	manager.todos[id] = in
}

func (manager *todoManager) Delete(id int) {
	delete(manager.todos, id)
}

func (manager todoManager) GetById(id int) (todo, error) {
	out, ok := manager.todos[id]
	if !ok {
		return todo{}, errors.New("no todo found for id " + strconv.Itoa(id))
	}

	return out, nil
}

func (manager todoManager) GetAll() []todo {
	out := []todo{}

	for _, v := range manager.todos {
		out = append(out, v)
	}

	slices.SortFunc(out, func(i todo, j todo) int {
		return i.ID - j.ID
	})

	return out
}

type httpManager struct {
	todoManager TodoManager
}

func NewHttpManager(todoManager TodoManager) httpManager {
	return httpManager{
		todoManager: todoManager,
	}
}

func (manager httpManager) GetById(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.PathValue("id"))
	if err != nil {
		// TODO
	}

	todo, err := manager.todoManager.GetById(id)
	if err != nil {
		// TODO
	}

	out, err := jsoniter.MarshalToString(todo)
	if err != nil {
		// TODO
	}

	w.Write([]byte(out))
}

func (manager httpManager) GetAll(w http.ResponseWriter, r *http.Request) {
	out, err := jsoniter.MarshalToString(manager.todoManager.GetAll())
	if err != nil {
		// TODO
	}

	w.Write([]byte(out))
}

func (manager httpManager) Set(w http.ResponseWriter, r *http.Request) {
	var in todo
	err := jsoniter.NewDecoder(r.Body).Decode(&in)
	if err != nil {
		// TODO
	}

	manager.todoManager.Set(in)

	w.Write([]byte(""))
}

func (manager httpManager) Update(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.PathValue("id"))
	if err != nil {
		// TODO
	}

	var in todo
	err = jsoniter.NewDecoder(r.Body).Decode(&in)
	if err != nil {
		// TODO
	}

	manager.todoManager.Update(id, in)

	w.Write([]byte(""))
}

func (manager httpManager) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.PathValue("id"))
	if err != nil {
		// TODO
	}

	manager.todoManager.Delete(id)

	w.Write([]byte(""))
}

func main() {
	manager := NewHttpManager(NewTodoManager())

	mux := http.NewServeMux()

	mux.HandleFunc("GET /todo/", manager.GetAll)

	mux.HandleFunc("GET /todo/{id}/", manager.GetById)

	mux.HandleFunc("POST /todo/", manager.Set)

	mux.HandleFunc("PUT /todo/{id}/", manager.Update)

	mux.HandleFunc("DELETE /todo/{id}/", manager.Delete)

	http.ListenAndServe("localhost:8080", mux)
}
